## FastHenryC99

This is an attempt to clean up the FastHenry2-Sourcecode as provided by
FastFieldSolvers S.R.L.

For the moment, there is one Branch named 'Fixes', which contains
corrections to the sourcecode which concern either incorrect behaviour
(including memory leaks) or wrong number/type of arguments passed to function

As a next step, in addition to continuing work on the 'Fixes' branch, two more
branches are planned:
- NoWarn: Provide function prototypes, remove unused variables etc. to get
          rid of warnings for contemporary compilers (for the moment: GCC
	  and CLANG-LLVM on linux) as far as reasonably possible.
- NoDead: In addition, also remove dead code.


